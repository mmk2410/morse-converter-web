/*
 * 2015 - 2016 (c) Marcel Kapfer (mmk2410)
 * Licensed under MIT License
 */
(function (document) {
  'use strict';

  var app = document.querySelector('#app');

  app.displayInstalledToast = function() {
    document.querySelector('#caching-complete').show();
  };

  app.addEventListener('template-bound', function() {
    console.log("Marcel Kapfer (mmk2410)");
    console.log("Morse Converter");
    console.log("Version 0.3.1");
  });

  window.addEventListener('WebComponentsReady', function() {
    document.querySelector('body').removeAttribute('unresolved');

    // Ensure that the drawer is always hidden
    var drawerPanel = document.querySelector('#paperDrawerPanel');
    drawerPanel.forceNarrow = true;

    // initialize the app
    var inputArea, menuAbout, menuFork, menuNormalMorse, menuReportBug, menuWrittenMorse, outputArea, outputPaper, toastNormalMorse, toastWrittenMorse, writtenMorse;
    writtenMorse = true;
    toastWrittenMorse = document.querySelector('#toastWrittenMorse');
    toastNormalMorse = document.querySelector('#toastNormalMorse');
    inputArea = document.querySelector('#inputArea');
    outputArea = document.querySelector('#outputArea');
    outputPaper = document.querySelector('#outputPaper');
    menuWrittenMorse = document.querySelector('#menuWrittenMorse');
    menuWrittenMorse.addEventListener('click', function() {
      writtenMorse = true;
      fadeOut(outputPaper);
      inputArea.value = "";
      drawerPanel.closeDrawer();
      toastWrittenMorse.toggle();
    });
    menuNormalMorse = document.querySelector('#menuNormalMorse');
    menuNormalMorse.addEventListener('click', function() {
      writtenMorse = false;
      fadeOut(outputPaper);
      inputArea.value = "";
      drawerPanel.closeDrawer();
      toastNormalMorse.toggle();
    });
    menuReportBug = document.querySelector('#menuReportBug');
    menuReportBug.addEventListener('click', function() {
      window.open("https://phab.mmk2410.org/maniphest/query/2c8tO32QJimD/", "_blank");
      drawerPanel.closeDrawer();
    });
    menuAbout = document.querySelector('#menuAbout');
    menuAbout.addEventListener('click', function() {
      window.open("https://marcel-kapfer.de/writtenmorse", "_blank");
      drawerPanel.closeDrawer();
    });
    menuFork = document.querySelector('#menuFork');
    menuFork.addEventListener('click', function() {
      window.open("https://gitlab.com/mmk2410/morse-converter-web/", "_blank");
      drawerPanel.closeDrawer();
    });
    inputArea.addEventListener('keyup', function() {
      var string;
      if (input.value.trim()) {
        if(outputPaper.style.display == 'none') {
          fadeIn(outputPaper);
        }
        if (writtenMorse) {
          if (isWrittenMorseCode(input.value)) {
            string = getWrittenMorseDecoded(input.value);
          } else {
            string = getWrittenMorseEncoded(input.value);
          }
        } else {
          if (isMorseCode(input.value)) {
            string = getMorseDecoded(input.value);
          } else {
            string = getMorseEncoded(input.value);
          }
        }
        outputArea.innerHTML = string;
      } else {
        fadeOut(outputPaper);
      }
    });
  });

})(document);

function fadeOut(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 10);
}

function fadeIn(element) {
    var op = 0.1;  // initial opacity
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}
