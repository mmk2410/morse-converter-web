/**
 * 2016 (c) Marcel Kapfer (mmk2410)
 * Licensed under MIT License
 */

/**
 * Methods for converting Morse and writtenMorse code.
 */
class Convert {

  // Normal alphabet.
  static List<String> alphabet = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', 'Ä', 'Ö', 'Ü', 'ß',
    '.', ',', ':', ';', '?', '!', '-', '_', '(', ')',
    '=', '+', '/', '@', "'", '\$', '\n', " ", '<br>'
  ];

  // Normal alphabet signal words.
  static List<String> alphabetExtra = [
    'LETTERSPACE', 'END OF WORK', 'ERROR', 'STARTING SIGNAL', 'ENDING SIGNAL',
    'UNDERSTOOD', 'WAIT', 'SOS', 'LETTER SPACE', 'WORD SPACE'
  ];

  // writtenMorse Alphabet.
  static List<String> writtenMorse = [
    '01', '1000', '1010', '100', '0', '0010',
    '110', '0000', '00', '0111', '101', '0100',
    '11', '10', '111', '0110', '1101', '010',
    '000', '1', '001', '0001', '011', '1001',
    '1011', '1100', '11111', '01111', '00111',
    '00011', '00001', '00000', '10000', '11000',
    '11100', '11110', '0101', '1110', '0011',
    '00011000', '010101', '110011', '111000',
    '101010', '001100', '101011', '100001', '001101',
    '10110', '101101', '10001', '01010', '10010',
    '011010', '011110', '0001001', '<br>', "+", '\n'
  ];

  // writtenMorse alphabet signal words.
  static List<String> writtenMorseExtra = [
    '#', '000101', '00000000', '10101', '01010', '00010', '01000',
    '000111000', '#', '+'
  ];

  // writtenMorse separators.
  static List<String> writtenMorseSeparators = ['#', '+'];

  // Morse alphabet.
  static List<String> morse = [
    '.-', '-...', '-.-.', '-..', '.', '..-.', '--.', '....', '..', '.---',
    '-.-', '.-..', '--', '-.', '---', '.--.', '--.-', '.-.', '...', '-', '..-',
    '...-', '.--', '-..-', '-.--', '--..', '-----', '.----', '..---', '...--',
    '....-', '.....', '-....', '--...', '---..', '----.', '.-.-', '---.', '..--',
    '...--...', '.-.-.-', '--..--', '---...', '-.-.-.', '..--..', '-.-.--',
    '-....-', '..--.-', '-.--.', '-.--.-', '-...-', '.-.-.', '-..-.', '.--.-.',
    '.----.', '...-..-', '<br>', '       ', '\n'
  ];

  // Morse alphabet signal words.
  static List<String> morseExtra = [
    '   ', '...-.-', '........', '-.-.-', '.-.-.', '...-.', '.-...',
    '...---...', '   ', '       '
  ];

  // Morse separators.
  static List<String> morseSeparators = ['   ', '       '];

  /**
   * Convert message to writtenMorse code.
   *
   * @param message Message to convert.
   * @return converted message.
   */
  static String getWrittenMorseEncoded(String message) {
    return _encode(
      message, writtenMorse, writtenMorseExtra, writtenMorseSeparators);
  }

  /**
   * Convert messgae from writtenMorse code.
   *
   * @param message Message to convert.
   * @return converted Message.
   */
  static String getWrittenMorseDecoded(String message) {
    return _decode(
      message, writtenMorse, writtenMorseExtra, writtenMorseSeparators);
  }

  /**
   * Convert messgae to Morse code.
   *
   * @param message Message to convert.
   * @return converted Message.
   */
  static String getMorseEncoded(String message) {
    return _encode(
      message, morse, morseExtra, morseSeparators).replaceAll(" ", "&nbsp;");
  }

  /**
   * Convert messgae from Morse code.
   *
   * @param message Message to convert.
   * @return converted Message.
   */
  static String getMorseDecoded(String message) {
    return _decode(
      message, morse, morseExtra, morseSeparators);
  }

  /**
   * Convert message to given alphabet.
   *
   * @param message Message to convert.
   * @param codeAlphabet Alphabet of the code to convert to.
   * @param codeAlphabetExtra Signal words of code to convert to.
   * @param separators Separators of code to convert to.
   * @return Converted message.
   */
  static String _encode(String message, List<String> codeAlphabet, List<String> codeAlphabetExtra, List<String> separators) {

    message = message.trim().toUpperCase();

    if (message.isEmpty) {
      return null;
    }

    if (alphabetExtra.contains(message)) {
      return codeAlphabetExtra[alphabetExtra.indexOf(message)];
    }

    StringBuffer converted = new StringBuffer();

    List<String> messageList = message.split('');

    for (int i = 0; i < messageList.length; i++) {
      String char = messageList[i];

      converted.write(
        codeAlphabet[alphabet.indexOf(char)]
      );

      if ((i < messageList.length - 1)
        && (char != " ")
        && (messageList[i + 1] != " ")
        ) {
        converted.write(separators[0]);
      }
    }

    return converted.toString().substring(0, converted.length);

  }

  /**
   * Convert message from given alphabet.
   *
   * @param message Message to convert.
   * @param codeAlphabet Alphabet of the code to convert from.
   * @param codeAlphabetExtra Signal words of code to convert from.
   * @param separators Separators of code to convert from.
   * @return Converted message.
   */
  static String _decode(String message, List<String> codeAlphabet, List<String> codeAlphabetExtra, List<String> separators) {

    message = message.trim().toUpperCase();

    if(message.isEmpty) {
      return null;
    }

    if(codeAlphabetExtra.contains(message)) {
      return alphabetExtra[codeAlphabetExtra.indexOf(message)];
    }

    StringBuffer converted = new StringBuffer();

    List<String> lineList = message.split("\n");

    for (String line in lineList) {

      List<String> wordList = line.split(separators[1]);

      for(int i = 0; i < wordList.length; i++) {
        String word = wordList[i];

        List<String> charList = word.split(separators[0]);

        for(String char in charList) {

          if (codeAlphabet.contains(char)) {
            converted.write(
              alphabet[codeAlphabet.indexOf(char)]
            );
          }

        }

        if (i < wordList.length - 1) {
          converted.write(" ");
        }

      }

      converted.write("\n");

    }

    return converted.toString().substring(0, converted.length - 1);

  }

}
