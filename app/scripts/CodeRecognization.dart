/**
 * 2016 (c) Marcel Kapfer (mmk2410)
 * Licensed under MIT License
 */

import 'Convert.dart';

/**
 * Detect code.
 */
class CodeRecognization {

  /**
   * Detect if message is Morse code.
   *
   * @param message string to be checked.
   * @return Boolean true if message is Morse code.
   */
  static bool isMorseCode(String message) {

    List<String> alphabet = new List.from(Convert.alphabet);
    alphabet = alphabet
      ..remove('.')
      ..remove('-')
      ..remove(' ');

    for (String char in message.toUpperCase().split('')) {
      if (alphabet.contains(char)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Detect if message is writtenMorse code.
   *
   * @param message string to be checked.
   * @return Boolean true if message is writtenMorse coe.
   */
  static bool isWrittenMorseCode(String message) {

    List<String> alphabet = new List.from(Convert.alphabet);
    alphabet = alphabet
      ..remove('0')
      ..remove('1')
      ..remove('#')
      ..remove('+');

    for (String char in message.toUpperCase().split('')) {
      if (alphabet.contains(char)) {
        return false;
      }
    }

    return true;
  }

}
