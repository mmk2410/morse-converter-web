/**
 * 2016 (c) Marcel Kapfer (mmk2410)
 * Licensed under MIT License
 */

/**
 * JS interopt file for making the Dart methods accessible in JavaScript.
 */

import 'dart:js' as js;

import 'Convert.dart';
import 'CodeRecognization.dart';

void main() {
  js.context['isMorseCode'] = (message) {
    return CodeRecognization.isMorseCode(message);
  };

  js.context['isWrittenMorseCode'] = (message) {
    return CodeRecognization.isWrittenMorseCode(message);
  };

  js.context['getWrittenMorseEncoded'] = (message) {
    return Convert.getWrittenMorseEncoded(message);
  };

  js.context['getWrittenMorseDecoded'] = (message) {
    return Convert.getWrittenMorseDecoded(message);
  };

  js.context['getMorseEncoded'] = (message) {
    return Convert.getMorseEncoded(message);
  };

  js.context['getMorseDecoded'] = (message) {
    return Convert.getMorseDecoded(message);
  };
}
