# [Morse Converter (web)](https://mmk2410.org//morseconverter)

This is a Morse and [writtenMorse](https://gitlab.com/mmk2410/writtenmorse-specs/wikis/Home) converter for the web. With mobile-first in mind I tried to create a beautiful converting experience. The webapp is designed following the [Material design](https://material.io/) specifications and with enhanced features (like instant converting) to simplify the converting time.

## Use it

The webapp is accessible at [mmk2410.org/morseconverter](https://mmk2410.org//morseconverter).

## Contribute

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Write your code and the tests for it.
4. Check if the code passes the tests. (`pub run test`). A code which does not passes the tests will not merged.
5. Commit your changes (`git commit -am 'Add some feature'`)
6. Push to the branch (`git push origin my-new-feature`)
7. Create new merge request

## Technologies

* Polymer and JavaScript for the front end.
* Dart for the converting engine.

## Build

You need the following programs installed on your computer:

```
npm, bower, gulp, dart2js
```

To see your changes run the first time:

```
npm install && bower install
```

Now change to `app/scripts/` and compile the `MorseConverter.dart`:

```
dart2js -m MorseConverter.dart -o MorseConverter.js
```

and then rebuild the webapp with:

```
gulp
```

The webapp is accessible in the `dist/` directory. A development server can be started with

```
gulp serve
```
