import "package:test/test.dart";

import '../app/scripts/CodeRecognization.dart';

void main() {

  group("writtenMorse", () {

    test("CodeRecognization.isMorse() check if text is morse code", () {
      expect(CodeRecognization.isMorseCode(".-..   ---   .-.   .   --"), isTrue);
    });

    test("CodeRecognization.isMorse() check if text is not morse code", () {
      expect(CodeRecognization.isMorseCode("message1238170"), isFalse);
    });

  });

}
