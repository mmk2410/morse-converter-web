# 2015 - 2016 (c) Marcel Kapfer (mmk2410)
# Licensed under MIT License
gulp = require 'gulp'
coffee = require 'gulp-coffee'
gutil = require 'gulp-util'
gloadplug = require 'gulp-load-plugins'
$ = gloadplug()
del = require 'del'
runSequence = require 'run-sequence'
merge = require 'merge-stream'
path = require 'path'
fs = require 'fs'
glob = require 'glob'
connect = require 'gulp-connect'


gulp.task 'coffee', () ->
  gulp.src 'app/scripts/*.coffee'
    .pipe coffee({bare: true}).on 'error', gutil.log
    .pipe gulp.dest 'app/scripts/'

gulp.task 'images', () ->
  gulp.src 'app/images/**/*'
    .pipe $.cache $.imagemin {
      progressive: true
      interlaced: true
    }
    .pipe gulp.dest 'dist/images'
    .pipe $.size {title: 'images'}

gulp.task 'copy', () ->
  app = gulp.src([
    'app/*',
    '!app/precache.json'
  ], {
    dot: true
  }).pipe gulp.dest 'dist'

  bower = gulp.src([
    'bower_components/**/*'
  ]).pipe gulp.dest 'dist/bower_components'

  elements = gulp.src([
    'app/elements/**/*.html'
  ]).pipe gulp.dest 'dist/elements'

  swBootstrap = gulp.src ['bower_components/platinum-sw/bootstrap/*.js']
    .pipe gulp.dest 'dist/elements/bootstrap'

  swToolbox = gulp.src ['bower_components/sw-toolbox/*.js']
    .pipe gulp.dest 'dist/sw-toolbox'

  vulcanized = gulp.src ['app/elements/elements.html']
    .pipe $.rename 'elements.vulcanized.html'
    .pipe gulp.dest 'dist/elements'

  merge(app, bower, elements, vulcanized, swBootstrap, swToolbox)
    .pipe $.size {title: 'copy'}

gulp.task 'html', () ->
  assets = $.useref.assets {searchPath: ['app', 'dist']}

  gulp.src ['app/**/*.html', '!app/elements/**/*.html']
    .pipe $.if '*.html', $.replace 'elements/elements.html',
    'elements/elements.vulcanized.html'
    .pipe assets
    .pipe $.if '*.js', $.uglify()
    .pipe assets.restore()
    .pipe $.useref()
    .pipe $.if '*.html', $.minifyHtml {
      quotes: true
      empty: true
      spare: true
    }
    .pipe gulp.dest 'dist'
    .pipe $.size {title: 'html'}

gulp.task 'vulcanize', () ->
  DEST_DIR = 'dist/elements'

  gulp.src 'dist/elements/elements.vulcanized.html'
    .pipe $.vulcanize {
      dest: DEST_DIR
      strip: true
      inlineCss: true
      inlineScripts: true
    }
    .pipe gulp.dest DEST_DIR
    .pipe $.size {title: 'vulcanize'}

gulp.task 'precache', (callback) ->
  dir = 'dist'

  glob '{elements,scripts}/**/*.*', {cwd: dir}, (error, files) ->
    if error then callback error
    else
      files.push 'index.html', './',
        'bower_components/webcomponentsjs/webcomponents.min.js'
      filePath = path.join dir, 'precache.json'
      fs.writeFile filePath, JSON.stringify(files), callback

gulp.task 'serve', (callback) ->
  connect.server({
    port: 8080
    root: 'dist'
  })

gulp.task 'clean', del.bind null, ['dist']

gulp.task 'default', ['clean'], (cb) ->
  runSequence 'coffee', 'copy',
    ['images', 'html'],
    'vulcanize', 'precache', cb

try
  require('require-dir')('tasks')
catch err
