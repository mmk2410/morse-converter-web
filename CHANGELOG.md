# Version 0.3.1 (01 June 2016)

- Updated Bower dependencies
- Ripple effect on drawer items

# Version 0.3.0 (30 May 2016)

- Convert engine completly rewritten in Dart
 - More efficient on larger texts
 - Unit tests

# Version 0.2.0

- All features also available in the Android and desktop app
- Complete rewrite with Polymer

# Version 0.1.0

- Initial release
